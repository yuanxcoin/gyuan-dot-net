﻿using Gyuan.Infrastructure.Watcher;
using System.Threading.Tasks;

namespace Gyuan.Infrastructure.Broker
{
    public interface IExchangeBroker : IWatchableService
    {
        Task<BrokerExchangeResult> CalcExchange(BrokerExchangeParams parameters);
        Task<BrokerExchangeResult> Exchange(BrokerExchangeParams parameters);
        Task<BrokerExchangeResult> ExchangeStatus(BrokerExchangeStatusParams parameters);

        Task<BrokerExchangeResult> ExchangeToStable(BrokerExchangeToStableParams parameters);
        Task<BrokerExchangeResult> ExchangeToStableStatus(BrokerExchangeStatusParams parameters);

        Task<BrokerParams> GetParams();
    }
}
