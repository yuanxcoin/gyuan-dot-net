﻿using System.Collections.Generic;

namespace Gyuan.Infrastructure.Broker
{
    public class BrokerExchangeResult
    {
        public string ExchangeId { get; set; }
        public PaymentStatus Status { get; set; }

        public PaymentStatus InTxStatus { get; set; }
        public string InTxStatusDescription { get; set; }

        public PaymentStatus OutTxStatus { get; set; }
        public string OutTxStatusDescription { get; set; }

        public string SellCurrency { get; set; } // currency (BTC), I want to sell
        public decimal SellAmount { get; set; } // amount BTC, I need to pay

        public string BuyCurrency { get; set; } // GRFT
        public decimal BuyAmount { get; set; } // how many Gyuans I will get

        public decimal SellToUsdRate { get; set; } // rate BTC to USD
        public decimal GyuanToUsdRate { get; set; } // rate GRFT to USD

        public decimal ExchangeBrokerFeeRate { get; set; } // 0.005 (0.5%)
        public decimal ExchangeBrokerFeeAmount { get; set; } // fee amount

        public string PayWalletAddress { get; set; } // addres to pay (BTC)
        public string GyuanWalletAddress { get; set; }
        public string GyuanPaymentId { get; set; }
        public int GyuanBlockNumber { get; set; }

        public List<EventItem> ProcessingEvents { get; set; }
    }
}
