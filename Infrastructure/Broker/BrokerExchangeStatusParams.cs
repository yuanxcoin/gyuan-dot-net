﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gyuan.Infrastructure.Broker
{
    public class BrokerExchangeStatusParams
    {
        public string ExchangeId { get; set; }
    }
}
