﻿namespace Gyuan.Infrastructure.Broker
{
    public class ExchangeBrokerConfiguration
    {
        public string Url { get; set; }
    }
}
