﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gyuan.Infrastructure.Gateway
{
    public class GatewayOnlineSaleResult
    {
        public string PaymentUrl { get; set; }
    }
}
