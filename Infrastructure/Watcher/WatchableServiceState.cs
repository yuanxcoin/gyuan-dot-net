﻿namespace Gyuan.Infrastructure.Watcher
{
    public enum WatchableServiceState
    {
        Undefined,
        OK,
        Warning,
        Error
    }
}
