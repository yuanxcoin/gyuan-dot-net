﻿using Newtonsoft.Json;

namespace Gyuan.DAPI
{
    public class DapiError
    {
        [JsonProperty(PropertyName = "code")]
        public int Code { get; set; }

        [JsonProperty(PropertyName = "message")]
        public string Message { get; set; }
    }
}
