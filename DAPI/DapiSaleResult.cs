﻿using Newtonsoft.Json;

namespace Gyuan.DAPI
{
    public class DapiSaleResult
    {
        [JsonProperty(PropertyName = "PaymentID")]
        public string PaymentId { get; set; }

        [JsonProperty(PropertyName = "BlockNumber")]
        public int BlockNumber { get; set; }
    }
}
