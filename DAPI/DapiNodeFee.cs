﻿namespace Gyuan.DAPI
{
    public class DapiNodeFee
    {
        public string Address { get; set; }
        public ulong Fee { get; set; }
    }
}
