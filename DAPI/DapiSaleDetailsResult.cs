﻿using Newtonsoft.Json;

namespace Gyuan.DAPI
{
    public class DapiSaleDetailsResult
    {
        [JsonProperty(PropertyName = "AuthSample")]
        public DapiNodeFee[] AuthSample { get; set; }

        [JsonProperty(PropertyName = "Details")]
        public string SaleDetails { get; set; }

    }
}
